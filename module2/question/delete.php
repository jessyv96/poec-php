<?php
// suppression de la question l'id est passé en paramètre d'url
if(isset($_GET['id'])){
  $id =$_GET['id'];

  $query = $db->prepare(
    'DELETE FROM question
     WHERE id= :id');
  $result = $query->execute(array(
    ':id' => intval($id),
  ));


  if ($result) {
    //suppression des réponses liées à la question
    // afin d'eviter des lignes orpheniles dans la table answer
    $query2 = $db->prepare('DELETE FROM answer WHERE id_question = :id_question');

    $result2 = $query2->execute(array(
      'id_question' => intval($id)
    ));

    (result2)
    // en cas de succès redirection vers la liste des questions
    ? header('location:?route=question/list')
    : print('La supression des réponses à échoué !');

  }
  else {
    //echec de la premiere requete SQL
    echo "<p>La suppression de la question à échoué</p>";
  }
}
?>
