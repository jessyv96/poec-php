<?php
function getAnswerById($answers, $id){
  //recherche une réponse identifiée dans un tableau de réponses
  $answer = NULL;
  foreach ($answers as $a) {
    if ($a->id == $id) {
      $answer = $a;
      break;
    }
  }
  return $answer;
}


  if (isset($_GET['id_question'])) {
    $id_question = $_GET['id_question'];

    $query = $db->prepare(
      'SELECT title
       FROM question
       WHERE id = :id_question
       ');
    $query->execute(array(
    ':id_question' => $id_question
  ));

    $title = $query->fetch(PDO::FETCH_OBJ)->title;

    //recuperation des reponse associées à la question traitée
    $query2 = $db->prepare(
      'SELECT id, body, correct
       FROM answer
       WHERE id_question = :id_question
       ');
    $query2->execute(array(
      ':id_question' => $id_question
    ));
    $answers = $query2->fetchAll(PDO::FETCH_OBJ);
    // var_dump($answers);
    //si on est en mode édition
    if (isset($_GET['edit']) && isset($_GET['id_answer'])) {
      $answerEdit = getAnswerById($answers, intval($_GET['id_answer']));
    }
}
//click sur bouton submit du formulaire d'insertion
if (isset($_POST['insert'])) {
  //formulaire d'ajout d'une reponse
  //enrigistrement en base de donnée

    $correct = 0;
  if (isset($_POST['correct'])) {
    $correct = 1;
  }

      $query = $db->prepare('INSERT INTO answer (body, correct, id_question)
                              VALUES (:body, :correct, :id_question)');
      //execution de la requete
      $result = $query->execute(array(
          ':body' => $_POST['body'],
          ':correct' => $correct,
          ':id_question' => intval($_POST['id_question']),
      ));
      ($result)
        ? header('location:?route=answer/manage&id_question='. $_POST['id_question'])
        : print('<p>L\'enregistrement à échoué </p>')
        ;
}
//récuperation des reponses associées à la question traitée

//click sur le bouton submit du formulaire de mise à jour
if (isset($_POST['update'])) {
      $correct = 0;

    if (isset($_POST['correct'])){
      $correct = 1;
    }
      $query = $db->prepare(
      'UPDATE answer
       SET body = :body, correct = :correct
       WHERE id = :id'
      );

      $result = $query->execute(array(
      ':body' => $_POST['body'],
      ':correct' => $correct,
      ':id' => $_POST['id_answer']
      ));

    //url de retour à la liste des reponses pour la question traitée
    $url = '?route=answer/manage&id_question='. $_POST['id_question'];
    ($result)
    ? header('location:' .$url)
    : print('La mise à jour de la réponse à échoué');

}

?>
<h2>Question <?= $title ?> </h2>
<div class="container">
    <div class="row">
      <div class="col-md-8">
        <?php if(sizeof($answers) == 0): ?>
          <p>Aucune n'est associé</p>
        <?php else: ?>
          <table class="table table-bordered table-striped">
              <?php $i = 0; //compteur ?>
                <?php foreach ($answers as $answer): ?>
                  <tr>
                    <td><?=++$i ?></td>
                    <td><?=$answer->body?></td>
                    <td><?php echo($answer->correct == 1) ? 'Bonne' : 'Mauvaise';?> &nbsp;réponse</td>
                    <td>
                      <?php
                        $urlDel = '?route=answer/delete&id_answer='.$answer->id;
                        $urlDel .= '&id_question='.$id_question;

                        $urlEdit = '?route=answer/manage&id_question='.$id_question;
                        $urlEdit .= '&edit=true&id_answer='.$answer->id;
                        //avec url edit on reste sur la meme page a la diff on a rejaouter un nouveau paramettre
                      ?>
                      <a class="btn btn-default"
                          href="<?= $urlEdit ?>">Modifier</a>
                      <a class="btn btn-danger"
                          href="<?= $urlDel ?>">Supprimer</a>
                    </td>
                  </tr>
                <?php endforeach; ?>
          </table>
        <?php endif ?>
      </div>

      <div class="col-md-4">
        <?php if (!isset($_GET['edit'])): ?>
          <h3>Ajouter une réponse</h3>

        <form method="POST">
          <div class="form-group">
              <label for="body">Texte</label>
              <textarea name="body"></textarea>
          </div>

          <div class="form-group">
              <label for="correct">Bonne réponse</label>
              <input type="checkbox" name="correct">
          </div>


              <input type="submit" name="insert" value="Enregistrer">
              <input type="hidden" name="id_question" value="<?= $id_question ?>">
        </form>
        <?php else: ?>

          <h3>Modifier une réponse</h3>

          <!-- Si le paramètre edit n'est pas dans l'URL, on afiche
                le formulaire d'ajout une d'une reponse
          -->

        <form method="POST">
          <div class="form-group">
              <label for="body">Texte</label>
              <textarea name="body" required><?=$answerEdit->body?></textarea>
          </div>
          <div class="form-group">
            <label for="correct">Bonne réponse</label>
                <?php if ($answerEdit->correct == 1): ?>
                    <input type="checkbox" name="correct" checked>
                <?php else: ?>
                    <input type="checkbox" name="correct">
                <?php endif; ?>
          </div>
              <input class="btn btn-primary" type="submit" name="update" value="Mettre à jour">

              <input type="hidden" name="id_question" value="<?= $id_question ?>">
              <input type="hidden" name="id_answer" value="<?=$answerEdit->id ?>">
              <?php
                $url = '?route=answer/manage&id_question='.$id_question;
              ?>
              <!-- Le lien annulé permet de quitter le mode édition(modificatiion)
                   les parametre 'edit' ET 'answer' sont retirer de URL -->
              <a class="btn btn-default" href="<?= $url ?>">Annuler</a>
        </form>
        <?php endif; ?>
      </div>

    </div>
</div>
