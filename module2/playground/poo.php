<?php
include('Color.php'); // inclusion de la classe color
include_once('Product.php');
include('Book.php');
include_once('Weapon.php');
// class (permet de produire des objets en serie) et objet sont les deux mots clé de la POO
// pour identifier une classe, on lui met un majuscule en premiere lettre (reconmmandation)
 class Player {

   //PROPRIÉTÉS
    public $firstname = "";
    public $lastname = "";
    public $team = "";
    public $age = NULL;

  //MÉTHODE (Comportement, Action) "erreur"
  public function getFullName() {
    return $this->firstname . '' . $this->lastname;
  }

  public function ageInTenYears() {
    return $this->age + 10;
  }

  public function ageInNbYears($nbYears) {
    return $this->age + $nbYears;
  }

  public function ageInTenYearsAlter() {
    $this->age += 10;
    return $this->age;
  }


 }
 $player1 = new Player();
 $player2 = new Player();
 $player3 = new Player();

 // $player1->firstname = "Andrea"; //accès en écriture
 // $player1->lastname = "Pirlo"; //accès en écriture
 //
 // echo $player1->getFullName();
 //
 // $player1->age = 36;
 // echo $player1->ageInTenYears(); // affiche 46
 // echo $player1->ageAfterNbYears(30); //affiche 66
 //
 // var_dump($player1);
 // var_dump($player2);
 // var_dump($player3);

 $color1 = new Color("red");
 $color2 = new Color("orange");

 //echo $color1->convertToHexa(); //#FF0000


 $product1 = new Product("Produit A");
 $product1->setPrice(14.6);
 $product1->setAvailable(true);
 // var_dump($product1);
 // echo $product1->test;
 //echo $product1->test2;
 // echo $product1->CONSTANTE_DE_CLASSE // ce n'est pas possible
 echo Product::CONSTANTE_DE_CLASSE;
 echo "<p></p>";

 $book1 = new Book();
 $book1->setPrice(5.99);
 $book1->setNbPages(450);
 $book1->setName('Charlie et la chocolaterie');
  // var_dump($book1);

 //echo $book1->test;
 // echo $book1->test2; //FATAL ERROR (propriété privée)
 echo "<p></p>";

 $weapon1 = new Weapon("Beretta 9mm");
 $weapon1->setPrice(150);
 $weapon1->setCategory(6);
 $weapon1->setName('Beretta 9mm');
 // var_dump($weapon1);
?>
