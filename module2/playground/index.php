<?php
  $db = new PDO('mysql:host=localhost;dbname=quizz', 'root', 'paris');
  //$db est objet de type PDO, il contient des propriété des
  //méthodes permettant d'interagir avec la DB
  // var_dump($db);

  // -> query();
  $sql = 'SELECT * FROM stagiaire';
  //$db ->query($sql);
  //var_dump($db)

  //fetch
  // ligne sql transformer en tableau php à la assoc et num
  foreach ($db->query($sql) as $s) {
    //formats possibles : PDO::FETCH_ASSOS, PDO::FETCH_NUM, PDO::FETCH_OBJ
    echo '<p>'.$s['nom'].'</p>';
    echo '<p>'.$s['1'].'</p>'; // même chose
  }

 ?>
