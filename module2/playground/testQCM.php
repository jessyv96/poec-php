<?php
include_once('../qcm/Question.php');
include_once('../qcm/Answer.php');

$question = new Question(14, "combien font 2+2 ?", 9, 1);

// var_dump($question);

$answer1 = new Answer(66, "8", 0, 14);
$answer2 = new Answer(67, "2", 1, 14);
$answer3 = new Answer(68, "78", 0, 14);

$question->addAnswer($answer1);
$question->addAnswer($answer2);
$question->addAnswer($answer3);

echo $question->getAnswers()[0]->getBody();
?>
