<?php
  include_once('Product.php');
  class Weapon extends Product {
    private $category = NULL;

    public function setCategory() {
      return $this->category;
    }

    public function getCategory($category) {
      $this->category = $category;
      return $this->category;
    }
  }
?>
