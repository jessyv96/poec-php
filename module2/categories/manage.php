<?php
include('./categories.php');

function getCategoryById($categories, $id){
  //recherche une réponse identifiée dans un tableau de réponses
  $category = NULL;
  foreach ($categories as $c) {
    if ($c->id == $id) {
      $category = $c;
      break;
    }
  }
  return $category;
}

// Recuperation de la liste des catégories ( PAS DE IF - IL FAUT JUSTE AFFICHER.)
$categories = getCategories($db);

  if (isset($_GET['edit']) && isset($_GET['id_category'])) {
    $categoryEdit = getCategoryById($categories, intval($_GET['id_category']));
  }


// ENREGISTREMENT D'UNE CATEGORIE
if(isset($_POST['insert'])) {

    $cond1 = $_POST['category'] != "";

      if ($cond1) {

        $query= $db->prepare(
          'INSERT INTO categories (category)
           VALUES  (:category)
          ');
        $result = $query->execute(array(
          ':category' => $_POST['category']
        ));
        // var_dump($_POST);
        if ($result) {
            header('location:?route=categories/manage');
          } else {
            echo '<p>L\'ajout de la catégorie a échoué</p>';
          }
      }
      else {
            echo '<p>La catégorie n\'a pas été renseignée</p>';
      }
}


if(isset($_POST['update'])) {

  $query = $db->prepare(
    'UPDATE categories
     SET category = :category
     WHERE id = :id
     ');

     $result = $query->execute(array(
     ':category' => $_POST['category'],
     ':id' => $_POST['id_category']
     ));
     ($result)
     ? header('location:?route=categories/manage')
     : print('La mise à jour a échoué');
}
 ?>


<h2>Éditer les catégories</h2>

<div class="container">
  <div class="row">
    <div class="col-md-9">
        <h3>Liste des catégories</3>
        <table class="table table-bordered">
          <tr>
            <th>#</th>
            <th>Catégorie</th>
            <th>Actions</th>
          </tr>
            <?php $i=0; ?>
            <?php foreach ($categories as $category): ?>
          <tr>
            <td><?= ++$i; ?></td>
            <td><?=$category->category?></td>
            <?php $urlDel = '?route=categories/delete&id=' .$category->id;
                  $urlEdit ='?route=categories/manage&edit=true&id_category='.$category->id;
            ?>
            <td>
              <a href="<?=$urlEdit?>" class="btn btn-default btn-xs">Modifier</a>
              <a href="<?=$urlDel ?>" class="btn btn-danger btn-xs">Supprimer</a></td>
          </tr>
        <?php endforeach; ?>
        </table>
    </div>
    <div class="col-md-3">
      <?php if (!isset($_GET['edit'])): ?>
          <h3>Ajout d'une catégorie</h3>
          <form class="form-group" method="POST">
            <label for="body">Entrez votre catégorie :</label>
            <input type="text" name="category" required>
            <input type="submit" name="insert" value="Enregistrer" class="btn btn-default btn-xs">
          </form>
      <?php else: ?>
          <h3>Modifier une catégorie</h3>
          <form class="form-group" method="POST">
            <label for="body">Entrez votre catégorie :</label>
            <input type="text" name="category" value="<?=$categoryEdit->category?>">
            <input type="submit" name="update" value="Mettre à jour" class="btn btn-info btn-xs">
            <input type="submit" name="Cancel" value="Cancel" class="btn btn-default btn-xs">
            <input type="hidden" name="id_category" value="<?=$categoryEdit->id ?>">
          </form>
      <?php endif; ?>
    </div>
  </div>

</div>
