<?php
include('routes.php');
$db = new PDO('mysql:host=localhost;dbname=quizz', 'root', 'paris');
//$db est objet de type PDO, il contient des propriété des
//méthodes permettant d'interagir avec la DB
// var_dump($db);

if (isset($_GET['route'])) {
  $route = $_GET['route'];
}
 ?>
 <!DOCTYPE html>
 <html>
   <head>
     <meta charset="utf-8">
     <title>Module 2: module quizz</title>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
   </head>
   <body>
     <header>
       <nav>
          <ul class="nav nav-tabs">
            <li><a href="?route=question/list">Liste des questions</a></li>
            <li><a href="?route=question/add">Ajout d'une question</a></li>
            <li><a href="?route=categories/manage">Catégories</a></li>
            <li><a href="?route=qcm">QCM</a></li>
          </ul>
       </nav>
     </header>
     <h1>Module 2: module quizz</h1>
     <!--Création d'une application quizz -->
    <?php
    if(isset($route)) {
      include($routes[$route]);
    }
    ?>
   </body>
 </html>
