<?php
  include('./categories.php');
  include('./levels.php');
  include('QCM.php'); // inclusion de la classe QCM
  $categories = getCategories($db);

  if (isset($_POST['submit'])) {
    //Création d'un objet QCM
    $qcm = new QCM(
      $db,
      $_POST['category'],
      $_POST['level'],
      $_POST['nb_questions']
    );
    $questions = $qcm->generate();
}

  if(isset($_POST['validate'])) {
    //on reconstitue l'objet $qcm 'perdu' en raison de la nouvelle requete HTTP
    //var_dump($_POST);
    $qcm = new QCM(
      $db,
      $_POST['category'],
      $_POST['level'],
      $_POST['nb_questions']
    );
    $questions = $qcm->generate();
    $qcm->processChoices($_POST);
  }
  // // var_dump($qcm);

  // // retourne un tableau
  // // echo "<pre>";
  // //     print_r($questions);
  // // echo "</pre>";



?>
<h3>Génération d'un QCM</h3>
<!-- filtres -->
<form class="form-inline well" method="POST">
  <div class="form-group">
    <select name="category">
      <option value="0">Choisir une catégorie :</option>
      <?php foreach ($categories as $category): ?>
        <option value="<?=$category->id?>"><?=$category->category?></option>
      <?php endforeach; ?>
    </select>
  </div>

  <div class="form-group">
    <select name="level">
      <option value="0">Choisir un niveau de difficulté :</option>
      <?php foreach ($levels as $key => $level): ?>
        <option value="<?=$key?>"><?=$level?></option>
      <?php endforeach; ?>
    </select>
  </div>

  <div class="form-group">
    <label for="nb_quesions">Nombre max de questions :</label>
    <input type="number" name="nb_questions">
  </div>

  <input type="submit" name="submit" value="Générer">
</form>

<?php if (isset($qcm) && !$questions) {
    echo '<div class="alert alert-warning">';
    echo 'Aucune question ne correspond aux critères de recherche';
    echo '</div>';
      }
 ?>


<?php if (isset($_POST['submit']) && $questions != false):?>
  <form class="" method="POST">
    <?php foreach ($questions as $question): ?>
      <div>
        <h4> <?= $question->getTitle(); ?></h4>
        <?php foreach ($question->getAnswers() as $answer): ?>
          <div>
            <input name="<?=$question->getId()?>[]" type="checkbox" value="<?= $answer->getId(); ?>">
            <?= $answer->getBody(); ?>
          </div>
        <?php endforeach; ?>
      </div>
    <?php endforeach; ?>
    <input type="hidden" name="category" value="<?= $qcm->getCategory() ?>">
    <input type="hidden" name="level" value="<?=$qcm->getlevel()?>">
    <input type="hidden" name="nb_questions" value="<?=$qcm->getNbQuestions()?>">
    <input type="submit" name="validate" value="Valider">
  </form>
<?php endif; ?>



</div>
