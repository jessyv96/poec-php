<?php
  include_once('./author/Author.php');
  include_once('./author/AuthorManager.php');

 //objet permettant les operation en DB
  $author_manager = new AuthorManager($db);

    //ajout d'un auteur
    if (isset($_POST['submit'])) {
      //creation d'un objet à partir des données postées
      $author = new Author(
        $_POST['firstname'],
        $_POST['lastname'],
        intval($_POST['birth_year']),
        $_POST['country']
      );
      var_dump($author);

      //on fournit l'objet $author au manager pour les opeation en DB
      // et teste le cas echec(== 0)
      if ($author_manager->save($author) == 0) echo "Echec de l'enregistrement";
    }

    //suppression d'un auteur
    if (isset($_GET['action'])) {
      $action = $_GET['action'];
      $author_id = intval($_GET['id']);

      if ($action == 'delete') {
        if ($author_manager->deleteById($author_id) == 0) echo "La suppression à échouer";
      }
    }

  $authors = $author_manager->list();
?>

<h2>Liste des auteurs</h2>

<form method="POST" class="form-inline">
  <div class="form-group">
    <input type="text" name="firstname" placeholder="prénom">
  </div>
  <div class="form-group">
    <input type="text" name="lastname" placeholder="nom">
  </div>
  <div class="form-group">
    <input type="number" name="birth_year" placeholder="Ex : 2017">
  </div>
  <div class="form-group">
    <input type="text" name="country" placeholder="Pays">
  </div>

    <input type="submit" name="submit" value="Enregistrer">

</form>
<!-- Liste des auteurs -->
<table class="table table-striped table-bordered">
  <tr>
    <th>Prénom</th>
    <th>nom</th>
    <th>Année de naissance</th>
    <th>pays</th>
    <th>actions</th>
  </tr>

</table>
