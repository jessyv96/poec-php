<?php
  ini_set('display_errors', 1);
  include('lib/functions.php');
  include('datasource.php');

  $stagiaires = listeStagiaires();
 ?>

<!DOCTYPE html>
<?php include('header.php'); ?>
  <h2>Liste stagiaire</h2>
    <div class="col-md-9">
      <table class="table table-striped table-bordered" id="tableau">
        <tr>
          <th>Prénom</th>
          <th>Nom</th>
          <th>Totem</th>
          <th>Dernière note</th>
          <th>Moyenne des notes</th>
        </tr>
        <?php
          foreach ($stagiaires as $s) {
            $moyenne = moyenne($s['notes'], 2);
            echo "<tr>";
                echo "<td>". majusculeInitiale($s["prenom"]) ."</td>";
                echo '<td><a href="stagiaire_info.php?id='. $s["id"].'">'. majusculeInitiale($s["nom"]) .'</a></td>';
                echo '<td><img src="'.ASSETS_PATH.'/img/images_etudiant/'. $s['totem'] .'" alt="">.</td>';
                //echo "<td>". $s['notes'] [sizeof($s['notes']) - 1] ."</td>";
                echo "<td>" . derniereNote($s['notes']) . "</td>";
                    if ($moyenne < 10 && $moyenne !== AUCUNE_NOTE_MSG) {
                        echo '<td class="echec">'. $moyenne . '</td>';
                    }
                    else if ($moyenne > 10 && $moyenne !== AUCUNE_NOTE_MSG) {
                        echo '<td class="success">'. $moyenne . '</td>';
                    } else {
                        echo '<td class="absent">'. $moyenne . '</td>';
                    }

            echo "</tr>";
          }
         ?>
      </table>
    </div>
        <div class="col-md-3">
          <?php include('sidebar.php'); ?>
        </div>

        </table>

<?php include('footer.php'); ?>
