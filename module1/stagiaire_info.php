<!--Assemblage de morceaux de DOM avec plusieurs fichiers (header.php et footer.php)-->

<?php
//ini_set('display_errors', 1);
include('header.php');
include('datasource.php');
include('lib/functions.php');
  //print_r($_GET); //afficher le contenu d'un tableau utilisé pour le debuggage  et echo affiche le contenu des chaine de caractère

  if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $stagiaire = stagiairesById($id);
    //var_dump($stagiaire); // donne des info detaillées de la variable

        if($stagiaire){ //si stagiaire n'est pas NULL, ni false, ni chaîne vide
          echo afficheStagiaireDetails($stagiaire);
        } else {
          echo "Stagiaire non trouvé ou inconnu";
        }

  } else {
    echo "paramètre id manquant";
  }
 ?>


<?php include('footer.php'); ?>
