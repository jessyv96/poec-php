<?php

  function majusculeInitiale($str) {
    //Dans la version actuelles les caractères accentués
    //ne sont pas convertis en majuscule

      $initiale = $str[0]; // équivalent : substr($str, 0, 1);
      $reste = substr($str, 1);
      $initialeMasjus = strtoupper($initiale);
      $resteMinus = strtolower($reste);

    return $initialeMasjus . $resteMinus;
}

  function  derniereNote($notes) {
    //renvoi la derniere note si le Tableau note n'est pas vide
    // renvoi "aucune note" si le tableau est vide
    $nb_note = sizeof($notes);

    if (sizeof($notes) == 0) {
      return AUCUNE_NOTE_MSG;
    }
    else {
      return $notes[$nb_note - 1];
    }
}

  function moyenne($notes, $precision) {
    $somme = 0;
    $nb_note = sizeof($notes);

    if ($nb_note == 0) return AUCUNE_NOTE_MSG; // accolade pas obligatoire si il y a une seule instruction
    if ($nb_note == 1) return $notes[0];

    foreach ($notes as $note) {
      $somme += $note; //equivalent à $somme = $somme + $nots
    }
    return round($somme / $nb_note, $precision);
}

  function afficheStagiaireDetails($stagiaire) {
    $output = '';
    $output .= '<div class="stagiaire">'; // le .= concatène la variable et ne l'écrase pas
    $output .= '<h2>' .$stagiaire['nom']. '</h2>';
    $output .= '<img src="' .ASSETS_PATH. '/img/images_etudiant/' .$stagiaire['totem']. '"  alt=""/>';
    $output .= '</div>';

    return $output;
  }

  function meilleurStagiaire($stagiaires) {
    //@IN une liste de stagiaires
    //@OUT le stagiaire avec la meilleur moyenne
    $meilleurMoyenne = moyenne($stagiaires[0]['notes'], 2); // le premier par defaut - on fait la moyenne du premier stagiaire de la liste
      //on fait des comparaison avec des boucles
      $meilleurStagiaire = NULL;
      $indice = NULL;
      $i = 0;
        foreach ($stagiaires as $s) {
          if (moyenne($s['notes'], 2) > $meilleurMoyenne) {
            $meilleurMoyenne = moyenne($s['notes'], 2);
            $meilleurStagiaire = $s;
            $indice = $i;
            }
          $i++;
        }  return array(
          'stagiaire' => $meilleurStagiaire,
          'moyenne' => $meilleurMoyenne,
          'indice' => $indice,
        );
    }

  function meilleursStagiaires($stagiaires, $limit){
      // @IN corresopond à la source de données
      // @IN l'argurment $limit qui definit le nombre de stagaire à renvoyer
      //@OUT le tableau des stagaires + la moyenne
      $i= 0;
      $meilleursStagiaires =  array();
      while($i < $limit){
        $meilleur = meilleurStagiaire($stagiaires);
        array_push($meilleursStagiaires, $meilleur);
        array_splice($stagiaires, $meilleur['indice'], 1);
        $i++;
      }
      return $meilleursStagiaires;
    }

    function verifIdentite($info, $datasource){
        //@IN info: $_POST
        //@IN datasouce: source de donnée dans laquelle on recherche
        //@OUT: true or false

        $found = false;
        foreach ($stagaires as $s) {
          if (($s['nom'] == $info['nom']) && ($s['password'] == $info['password'])) {
            $found = true;
            break;
          }
        }
    }
?>
