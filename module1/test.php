<?php
  include('lib/functions.php');
  ini_set('display_errors', 1); //erreur affichées dans le navigateur
?>

<?php
  //TEST DE LA FONCTION
  echo moyenne([8,12], 2);
  echo moyenne([8,12, 2], 2);
  echo moyenne([], 0);

 ?>
<p>paragraphe</p>
<?php
    $v ="bonjour";//string
    echo majusculeInitiale($v);
    echo gettype($v);
    $v = 14; // number
    echo $v;
    echo gettype($v);
    $v = 10.4; //float
    echo $v;
    echo gettype($v);
    $v = true; //boolean
    echo $v;
    echo gettype($v);
    $v2; // NULL
    echo gettype($v2);
    //operation sure intenger
    $nb1 = 45;
    $nb2 = 5;
    $nb3 = "3";

    echo $nb1 * $nb2;
    echo $nb1 * $nb3; // conversion implicitite de $nb3 en integer

    $string = "Un tiens vaut mieux";
    $string2 = "que deux tu l'auras";

    //TYPE COMPLEXES
        //Tableau à indice numerique (commence à 0)
    $tableau = [];
    $tableau2 = array();
    echo gettype($tableau);
    echo gettype($tableau2);

    $etudiants = ["etudiant1", "etudiant2", "etudiant3"];
    echo $etudiants [2]; //Doit retourner etudiant3
    $etudiants [0] = "Marc";
    echo $etudiants[0];

    $mix = ["chaine", 45, false, NULL, $etudiants];
    echo $mix [4][0]; // tableau imbriqué à deux dimentions

        //tableaux associatifs
    $joueurs = array(
      'joueur1' => array(
        'nom' => 'Messi',
        'prenom' =>'Lionel',
        'maillot' => 10
      )
    );
      echo $joueurs ['joueur1']['prenom'];

      $j1 = array("prenom" => "Paolo", "nom" => "Dybala", "maillot" => 10);
      $j2 = array("prenom" => "Giorgio", "nom" => "Chiellini", "maillot" => 3);
      $j3 = array("prenom" => "Andrea", "nom" => "Barzagli", "maillot" => 15);

      $juve = array($j1, $j2, $j3);

      $j1["maillot"] = 21; //
      echo $j1["maillot"];
      $juve [0]["maillot"] = 21; //Change dans tout le programme
      $juve [1]['maillot'] = 45;
      echo $juve [1]['maillot'];
      echo $juve [0]['maillot'];

      //Structure intérative
          //Boucle for
      echo '<ul>';
      for($i=0; $i < sizeof($juve); $i++) {
        echo'<li>'.$juve[$i]['prenom'].' '.$juve[$i]['nom'].'</li>';
      }
      echo '</ul>';

          //boucle while
      echo "<select>";
      $compteur = 0;
        while ($compteur < sizeof($juve)) {
          echo '<option>'. $juve[$compteur]['maillot']. '</option>';
          $compteur++;
        }
      echo "</select>";


          //boucle foreach
      foreach ($juve as $j) {
        if ($j['maillot'] == 21) {
          echo "<p style='color:green;'>". $j['nom']. ' (meneur de jeu)</p>';
          }
        else {
          echo "<p>". $j['nom']. '</p>';
          }

      }
?>
