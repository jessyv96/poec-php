<?php
  include_once('datasource.php'); //include_once signifie
  include_once('lib/functions.php');


  $meilleurs = meilleursStagiaires(listeStagiaires(), 3);

  ?>

  <ul>
    <?php
    $i=0;
      foreach ($meilleurs as $m) {
        if ($i == 0) {
          echo '<li class="boss">' .$m['stagiaire']['nom']. '('.$m['moyenne'].')</li>';
        }
        else {
          echo '<li>' .$m['stagiaire']['nom']. '('.$m['moyenne'].')</li>';
        }
        $i++;
      }
     ?>
  </ul>
