<h2>Book</h2>
<?php

class Book extends BliblioApp {
  private $idBook = NULL;
  private $title = NULL;
  private $nbPages = NULL;
  private $author = NULL;
  private $isbn = NULL;

  public function __construct($idBook, $title, $nbPages, $author, $isbn) {

  }

  public function getIdBook() {return $this->idBook;}
  public function getTitle() {return $this->title;}
  public function getNbPages() {return $this->nbPages;}
  public function getAuthor() {return $this->author;}
  public function getIsbn() {return $this->isbn;}
}
 ?>
