<h2>Author</h2>
<?php
  class Author extends BliblioApp {

    private $idAuthor = NULL;
    private $firstName = NULL;
    private $lastName = NULL;
    private $birthYear = NULL;
    private $country = NULL;

    public function __construct($idAuthor, $firstName, $lastName, $birthYear, $country) {

    }

    //getters
    public function getIdAuthor() {return $this->idAuthor;}
    public function getFirstName() {return $this->firstName;}
    public function getLastName() {return $this->lastName;}
    public function getBirthYear() {return $this->birthYear;}
    public function getCountry()  {return $this->country;}
  }

?>
