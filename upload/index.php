<?php
  //var_dump($_POST);
  var_dump($_FILES);
  /*
    $_FILES: Tableau de tableau asossciatif, chaque tableau assos
    représente un ficher "uploaded" contenant les clés suivantes :
      - name;
      - type
      - tmp_name
      - error
      - size
  */
  $dir_upload = 'images_uploaded';

  if (isset($_POST['submit'])) {

    // To do: verifier type et size avant telechargment
    
      $result = move_uploaded_file($_FILES['file']['tmp_name'],
      $dir_upload . $_FILES['file']['name']);

      ($result)
      ? print("Le téléchargement a reussi")
      : print("Le téléchargement a echoué");

  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Upload</title>
  </head>
  <body>
    <form method="post" enctype="multipart/form-data">
      <input type="file" name="file">
      <input type="submit" name="submit" value="Envoyer les données">
    </form>
  </body>
</html>
